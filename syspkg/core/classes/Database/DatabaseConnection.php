<?php
namespace Dub\Core\Database;

/**
 * Database connection wrapper class
 */
class DatabaseConnection {
	private $pdo;
	
	/**
	 * Constructor for creating the connection
	 * 
	 * @param string $host
	 * @param string $username
	 * @param string $password
	 * @param string $database
	 */
	public function __construct($driver, $host, $username, $password, $database) {
		$this -> pdo = new \PDO(
			$driver . ':host=' . $host . ';dbname=' . $database,
			$username,
			$password
		);
	}

	/**
	 * Close PDO Connection just for being sure it is closed
	 */
	public function __destruct() {
		$this -> pdo = null;
	}
	
	/**
	 * Execute a select statement
	 * 
	 * @param string $table
	 * @param string $fields
	 * @param string $where
	 * @param array $values
	 * @param string $orderBy
	 *
	 * @return array The query result
	 */
	public function select($table, $fields = '*', $where = null, $whereValues = array(), $orderBy = null, $showDeleted = 0, $single = false) {
		$query = 'SELECT ' . $fields . ' FROM ' . $table . ' WHERE deleted = ' . $showDeleted
		. ($where ? ' AND ' . $where : '')
		. ($orderBy ? ' ORDER BY ' . $orderBy : '') . ';';
		$stmt = $this -> pdo -> prepare($query);
		$stmt -> execute($whereValues);
		
		if($single) {
			return $stmt -> fetch(\PDO::FETCH_ASSOC);
		}
		
		return $stmt -> fetchAll(\PDO::FETCH_ASSOC);
	}
	
	/**
	 * Execute a select statement with joins
	 * 
	 * @param string $table
	 * @param string $fields
	 * @param string $where
	 * @param array $values
	 * @param string $orderBy
	 *
	 * @return array The query result
	 */
	public function selectMm($table, $fields = '', $join = null, $where = null, $whereValues = array(), $orderBy = null, $showDeleted = 0) {
		$query = 'SELECT ' . $fields . ' FROM ' . $table
		. ($join ? ' JOIN ' . $join : '')
		. ' WHERE ' . $table . '.deleted = ' . $showDeleted
		. ($where ? ' AND ' . $where : '')
		. ($orderBy ? ' ORDER BY ' . $orderBy : '') . ';';
		$stmt = $this -> pdo -> prepare($query);
		$stmt -> execute($whereValues);
		
		return $stmt -> fetchAll(\PDO::FETCH_ASSOC);
	}
	
	/**
	 * Execute a select statement but only return the first row
	 * 
	 * @param string $table
	 * @param string $fields
	 * @param string $where
	 * @param array $values
	 * @param string $orderBy
	 *
	 * @return array The first row of the query result
	 */
	public function selectSingle($table, $fields = '*', $where = null, $whereValues = array(), $orderBy = null, $showDeleted = 0) {
		return $this -> select($table, $fields, $where, $whereValues, $orderBy, $showDeleted, true);
	}
	
	/**
	 * Execute an insert statement by a given associative array
	 * 
	 * @param string $table
	 * @param array $values Associative arrays with the field names as key
	 *
	 * @return int Uid of the inserted row or false if not inserted
	 */
	public function insert($table, $values = array()) {
		if($values) {
			$query = 'INSERT INTO ' . $table . ' (' . implode(',', array_keys($values))
			. ') VALUES (:' . implode(',:', array_keys($values)) . ');';
			$stmt = $this -> pdo -> prepare($query);
			$inserted = $stmt -> execute($values);

			if($inserted) {
				return $this -> pdo -> lastInsertId();
			}
		}

		return false;
	}
	
	/**
	 * Execute an update statement
	 * 
	 * @param string $table
	 * @param string $field
	 * @param string $value
	 * @param string $where
	 *
	 * @return bool Success
	 */
	public function update($table, $field, $value, $where = null, $whereValues = array()) {
		$query = 'UPDATE ' . $table . ' SET ' . $field . ' = :' . $field
		. ($where ? ' WHERE ' . $where : '') . ';';
		$stmt = $this -> pdo -> prepare($query);
		$stmt -> bindParam(':' . $field, $value);
		
		return $stmt -> execute($whereValues);
	}
	
	/**
	 * Execute a delete statement
	 * 
	 * @param string $table
	 * @param string $where
	 *
	 * @return bool Success
	 */
	/*public function delete($table, $where = null, $whereValues = array()) {
		$query = 'UPDATE ' . $table . ' SET deleted = 1 ' . ($where ? ' WHERE ' . $where : '') . ';';
		$stmt = $this -> pdo -> prepare($query);
		
		return $stmt -> execute($whereValues);
	}*/
	
	/**
	 * Execute a delete statement
	 * 
	 * @param string $table
	 * @param string $where
	 *
	 * @return bool Success
	 */
	public function delete($table, $where = null, $whereValues = array()) {
		$query = 'DELETE FROM ' . $table . ($where ? ' WHERE ' . $where : '') . ';';
		$stmt = $this -> pdo -> prepare($query);
		
		return $stmt -> execute($whereValues);
	}
}
