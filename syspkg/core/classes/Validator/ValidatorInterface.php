<?php
namespace Dub\Core\Validator;

// Please ignore
interface ValidatorInterface {
	public function isValid($value, $options = array());
}
