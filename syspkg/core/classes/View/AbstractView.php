<?php
namespace Dub\Core\View;

/**
 * This is the abstract view. All different types of views should extend it.
 * It gives the functionality to assign, remove and get a variable to and from a view,
 * so you can output it in the template. It also offers the functionality to
 * Render a package-, controller- and action-dependent template, where the controller
 * and action don't really have to exist to give you some more flexibility.
 * The other functionality it gives is the rendering of a partial dependent on
 * a package.
 * On both, the templates and partials you can provide an associative array to use
 * them as normal variables inside the template. Example:
 *
 * Controller:
 * 	   $view -> assign(array('test', 'Hello world'));
 * Template/Partial:
 *     <?= $this -> get('test') ?>
 * Output:
 *     Hello world
 */
abstract class AbstractView {
	/**
	 * @var $variables the variables assigned to the view
	 */
	private $variables = array();
	
	/**
	 * Assign a variable to the view
	 * 
	 * @param mixed $key
	 * @param mixed $value
	 */
	public function assign($key, $value) {
		$this -> variables[$key] = $value;
	}
	
	/**
	 * Remove a variable from the view
	 * 
	 * @param mixed $key
	 */
	public function remove($key) {
		if(isset($this -> variables[$key])) {
			unset($this -> variables[$key]);
		}
	}
	
	/**
	 * Return a variable from the view
	 * 
	 * @param mixed $key
	 * 
	 * @return mixed
	 */
	public function get($key) {
	    if(isset($this -> variables[$key])) {
		    return $this -> variables[$key];
	    }
	    return null;
	}
	
	/**
	 * Render a template based on the package, controller and controller
	 * action and it's "arguments"
	 * 
	 * @param mixed $key
	 * 
	 * @return mixed
	 */
	public function renderTemplate($package, $controller, $action, $arguments = array()) {
		if($GLOBALS['dub']['packages'][$package]['active']) {
			$filename = $GLOBALS['dub']['packages'][lcfirst($package)]['packageFolder']
			. DS . 'templates' . DS . lcfirst($controller) . DS . $action . '.php';

			$this -> renderFileAsPhp($filename, $arguments);
		}
	}
	
	/**
	 * Render a partial based on the package, the name and its "arguments",
	 * for example if it is used in a foreach loop
	 * 
	 * @param mixed $key
	 * 
	 * @return mixed
	 */
	public function renderPartial($package, $name, $arguments = array()) {
		if($GLOBALS['dub']['packages'][$package]['active']) {
			$filename = $GLOBALS['dub']['packages'][lcfirst($package)]['packageFolder']
			. DS . 'partials' . DS . $name . '.php';
			
			$this -> renderFileAsPhp($filename, $arguments);
		}
	}

	/**
	 * Going to cheat and call this rendering^^ It is just an include, 
	 * what infact parses / renders the php inside of the file^^
	 * 
	 * @param string $filename
	 */
	public function renderFileAsPhp($filename, $arguments = array()) {
		extract($arguments);
		unset($arguments);
		
		$filename = DOCUMENT_ROOT . $filename;

		if(file_exists($filename)) {
			require $filename;
		}
	}
}
