<?php
namespace Dub\Core\View;

/**
 * The package view is used for displaying package templates and partials.
 * It holds a reference to the main view object and stores the name of the
 * package it is used for
 */
class PackageView extends AbstractView {
	private $mainView;
	private $package;
	
	public function __construct($mainView, $package) {
		$this -> mainView = $mainView;
		$this -> package = $package;
	}
}
