<?php
namespace Dub\Core\View;

/**
 * The main view holds all the package views and is able to 
 * render the according template
 */
class MainView extends AbstractView {
	private $packageViews = array();

	/**
	 * Append a new package view for a specific controller
	 *
	 * @param string $controller
	 * 
	 * @return View The appended view
	 */
	public function createPackageView($package) {
		$package = lcfirst($package);
		$this -> packageViews[$package] = new \Dub\Core\View\PackageView($this, $package);
		return $this -> packageViews[$package];
	}

	/**
	 * Render a package view
	 *
	 * @param string $package
	 * @param string $controller
	 * @param string $action
	 * @param array $arguments
	 */
	public function renderPackageTemplate($package, $controller, $action, $arguments = array()) {
		$this -> packageViews[lcfirst($package)] -> renderTemplate(
			$package,
			$controller,
			$action,
			$arguments
		);
	}
}
