<?php
namespace Dub\Core\Autoloader;

interface AutoloaderInterface {
	public function register();
}
