<?php
namespace Dub\Core\Autoloader;

// Include the abstract autoloader (This is only needed in classes loaded before the packages)
require_once DOCUMENT_ROOT . 'dub-cms' . DS . 'syspkg' . DS . 'core'. DS
	. 'classes' . DS . 'Autoloader' . DS . 'AutoloaderInterface.php';

class ClassesAutoloader implements AutoloaderInterface {
	public function register() {
		spl_autoload_register(function($classname) {
			$namespaces = explode('\\', $classname);
			$packagename = lcfirst($namespaces[1]);
			
			if($GLOBALS['dub']['packages'][$packagename]['active']) {
				$classFilename = DOCUMENT_ROOT . $GLOBALS['dub']['packages'][$packagename]['packageFolder']
				. DS . $GLOBALS['dub']['packages'][$packagename]['classesFolder'];
				
				array_shift($namespaces);
				array_shift($namespaces);
			
				foreach($namespaces as $namespace) {
					$classFilename .= DS . $namespace;
				}
			
				$classFilename .= '.php';
				
				if(file_exists($classFilename)) {
					require_once $classFilename;
				}
			}
		});
	}
}
