<?php
namespace Dub\Core\Controller;

/**
 * Basic Controller class holding a view and a request made of the get and post vars
 * Abstract because you must not instantiate it
 */
abstract class AbstractController {
	protected $view;
	protected $request;
	protected $errors = array();
	
	/**
	 * Constructor
	 * 
	 * @param \Dub\Core\View\PackageView $view The View
	 */
	public function __construct($view) {
		$this -> view = $view;
		$this -> request = array_merge($_GET, $_POST); // UNSAFE
		
		/**
		 * Run the init method, if exists. This is only that you don't have to
		 * create a constructor and call parent::__construct($view) every time.
		 * Because you should not have to implement it, it is not an abstract method
		 */
		if(method_exists($this, 'init')) {
			$this -> init();
		}
	}
}
