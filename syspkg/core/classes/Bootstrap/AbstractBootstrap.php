<?php
namespace Dub\Core\Bootstrap;

if(!defined('DS')) {
	define('DS', DIRECTORY_SEPARATOR);
}

// Include the baseclass (This is only needed in classes loaded before the packages)
require_once DOCUMENT_ROOT . 'dub-cms' . DS . 'syspkg' . DS . 'core' . DS
	. 'classes' . DS . 'Application' . DS . 'ApplicationInterface.php';

// Include the classes autoloader (This is only needed in classes loaded before the packages)
require_once DOCUMENT_ROOT . 'dub-cms' . DS . 'syspkg' . DS . 'core' . DS
	. 'classes' . DS . 'Autoloader' . DS . 'ClassesAutoloader.php';

/**
 * Bootstrap class
 */
abstract class AbstractBootstrap {
	public abstract function boot();

	/**
	 * Load the packages
	 * 
	 * @return Bootstrap This object for chaining
	 */
	protected function loadPackages() {
		$GLOBALS['dub']['packages'] = require_once DOCUMENT_ROOT . 'dub-cms-conf' . DS . 'packages.php';
		return $this;
	}
	
	/**
	 * Autoload classes
	 * 
	 * @return Bootstrap This object for chaining
	 */
	protected function autoloadClasses() {
		$classesAutoloader = new \Dub\Core\Autoloader\ClassesAutoloader();
		$classesAutoloader -> register();
		return $this;
	}
	
	/**
	 * Load the global functions
	 * 
	 * @return Bootstrap This object for chaining
	 */
	protected function loadGlobalFunctions() {
		require_once DOCUMENT_ROOT . $GLOBALS['dub']['packages']['core']['packageFolder'] . DS . 'globalFunctions.php';
		return $this;
	}
	
	/**
	 * Load the local CMS configuration
	 * TODO: MAKE A CMS :)
	 * 
	 * @return Bootstrap This object for chaining
	 */
	protected function loadLocalconf() {
		$GLOBALS['dub']['localconf'] = require_once DOCUMENT_ROOT . 'dub-cms-conf' . DS . 'localconf.php';
		return $this;
	}
	
	/**
	 * Load the database connection
	 * 
	 * @return Bootstrap This object for chaining
	 */
	protected function loadDatabaseConnection() {
		$GLOBALS['dub']['dbConnection'] = new \Dub\Core\Database\DatabaseConnection(
			$GLOBALS['dub']['localconf']['db']['driver'],
			$GLOBALS['dub']['localconf']['db']['hostname'],
			$GLOBALS['dub']['localconf']['db']['username'],
			$GLOBALS['dub']['localconf']['db']['password'],
			$GLOBALS['dub']['localconf']['db']['dbname']
		);
		return $this;
	}

	/**
	 * Load the packages localconf files
	 * 
	 * @return Bootstrap This object for chaining
	 */
	protected function loadPackagesLocalconf() {
		foreach ($GLOBALS['dub']['packages'] as $packageName => $package) {
			if($package['active']) {
				$packageLocalconfFilename = DOCUMENT_ROOT . $package['packageFolder'] . DS . 'localconf.php';

				if(file_exists($packageLocalconfFilename)) {
					$GLOBALS['dub']['packages'][$packageName]['localconf'] = 
					require_once $packageLocalconfFilename;
				}
			}
		}
		return $this;
	}
}
