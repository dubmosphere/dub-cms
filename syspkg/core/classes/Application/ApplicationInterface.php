<?php
namespace Dub\Core\Application;

/**
 * The application interface
 */
interface ApplicationInterface {
	public function run();
}
