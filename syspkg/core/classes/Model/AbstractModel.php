<?php
namespace Dub\Core\Model;

/**
 * Base class for all models
 */
abstract class AbstractModel  {
	protected $db;
	protected $table = '';
	
	/**
	 * The constructor assignes the database connection
	 */
	public function __construct() {
		$this -> db = $GLOBALS['dub']['dbConnection'];
	}

	/**
	 * Find model by uid
	 * 
	 * @param int $uid
	 * 
	 * @return array The query result
	 */
	public function findByUid($uid) {
		return $this -> db -> selectSingle($this -> table, '*', 'uid = ?', array($uid));
	}

	/**
	 * Find all models
	 * 
	 * @return array The query result
	 */
	public function findAll() {
		return $this -> db -> select($this -> table, '*');
	}

	/**
	 * Insert a model
	 * 
	 * @param array $model
	 * 
	 * @return int The inserted uid
	 */
	public function insert($model = array()) {
		return $this -> db -> insert($this -> table, $model);
	}

    /**
     * Delete a model by uid
     * 
     * @param array $modelUid
     * 
     * @return bool Success
     */
    public function delete($modelUid) {
        return $this -> db -> delete($this -> table, 'uid = ?', array($modelUid));
    }
}
