<?php
namespace Dub\Core\Utility;

class LinkUtility {
	/**
	 * Redirect to an url to a package controller action
	 * 
	 * @param string $package
	 * @param string $controller
	 * @param string $action
	 * @param array $actionParams
	 * @param bool $asGetParams
	 */
	public static function redirect($package, $controller = null, $action = null, $actionParams = array(), $asGetParams = false) {
		header('Location: ' . LinkUtility::buildUrl($package, $controller, $action, $actionParams, $asGetParams));
	}
	
	/**
	 * Redirect to an url to a package controller action
	 * 
	 * @param string $url
	 */
	public static function redirectByUrl($url) {
		header('Location: ' . $url);
	}

	/**
	 * Builds the url for a package controller action
	 * 
	 * @param string $package
	 * @param string $controller
	 * @param string $action
	 * @param array $actionParams
	 * @param bool $asGetParams
	 * 
	 * @return string $url
	 */
	public static function buildUrl($package, $controller, $action, $actionParams = array(), $asGetParams = false) {
		$actionParamsString = '';
		$url = '';

		if(!$asGetParams) {
			$url = '/' . $package;
			
			if($controller) {
				 $url .= '/' . $controller;
			}
			
			if($action) {
				$url .= '/' . $action;
			}
			
			foreach($actionParams as $actionParam) {
				$actionParamsString .= '/' . $actionParam;
			}
		} else {
			$url = '?';
			
			if($controller) {
				$url .= $package . '[controller]=' . $controller;
			}
			
			if($action) {
				$url .=  (strlen($url) === 1 ? '' : '&') . $package . '[action]=' . $action;
			}
			
			foreach($actionParams as $actionParam) {
				$actionParamsString .= ((strlen($url) === 1 && strlen($actionParamsString) === 0) ? '' : '&') . $package . '[actionParams][]=' . $actionParam;
			}
		}

		$url .= $actionParamsString;

		return $url;
	}
}
