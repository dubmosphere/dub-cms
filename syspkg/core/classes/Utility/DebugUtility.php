<?php
namespace Dub\Core\Utility;

class DebugUtility {
	public static function var_dump($variable, $name = '*variable*') {
		$view = new \Dub\Core\View\StandaloneView();
		$view -> assign('variable', $variable);
		$view -> assign('name', $name);
		$view -> renderTemplate('core', 'Debug', 'varDump');
	}
}
