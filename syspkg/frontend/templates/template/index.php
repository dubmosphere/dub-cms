<!DOCTYPE html>
<html lang="<?= $this -> get('language') ?>">
	<head>
		<?= $this -> get('metaData') ?>
		<?php $this -> renderTemplate('template', 'mainTemplate', 'head'); ?>
		<?= $this -> get('cssFiles') ?>
		<?= $this -> get('jsFilesHead') ?>
	</head>
	<body>
		<?php $this -> renderTemplate('template', 'mainTemplate', 'body'); ?>
		<?= $this -> get('jsFilesBody') ?>
	</body>
</html>