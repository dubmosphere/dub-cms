<?php
namespace Dub\Frontend\Application;

// Include the frontend bootstrap class (This is only needed in classes loaded before the packages)
require_once DOCUMENT_ROOT . 'dub-cms' . DS . 'syspkg' . DS . 'frontend' . DS
	. 'classes' . DS . 'Bootstrap' . DS . 'FrontendBootstrap.php';

/**
 * The application class
 */
class FrontendApplication implements \Dub\Core\Application\ApplicationInterface {
	protected $bootstrap;

	/**
	 * Runs the application (Bootstraps everything and creates the main view)
	 */
	public function run() {
		$this -> bootstrap = new \Dub\Frontend\Bootstrap\FrontendBootstrap();
		$this -> bootstrap -> boot();

		// TODO: Check where to better start the session
		session_start();

		$frontendFactory = new \Dub\Frontend\Factory\FrontendFactory();
		$frontendFactory -> createMainView();
	}
}
