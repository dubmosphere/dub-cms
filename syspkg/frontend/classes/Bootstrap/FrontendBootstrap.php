<?php
namespace Dub\Frontend\Bootstrap;

// Include the bootstrap interface (This is only needed in classes loaded before the packages)
require_once DOCUMENT_ROOT . 'dub-cms' . DS . 'syspkg' . DS . 'core' . DS . 'classes'
	. DS . 'Bootstrap' . DS . 'AbstractBootstrap.php';

/**
 * "Frontend" Bootstrap class loads everything necessary to the page
 */
class FrontendBootstrap extends \Dub\Core\Bootstrap\AbstractBootstrap {
	public function boot() {
		$this -> loadPackages()
			  -> autoloadClasses()
		      -> loadGlobalFunctions()
			  -> loadLocalconf()
			  -> loadDatabaseConnection()
			  -> loadPackagesLocalconf();
	}
}
