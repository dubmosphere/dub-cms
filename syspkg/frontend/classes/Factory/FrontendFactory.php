<?php
namespace Dub\Frontend\Factory;

/**
 * Package Factory creating the mainview and the package views
 * (I don't know if it really is a factory... but it creates things)
 */
class FrontendFactory {
	protected $request = array();
	protected $mainView = null;
	protected $packageInfo = array();
	protected $cssFiles;
	protected $jsFilesHead;
	protected $jsFilesBody;

	/**
	 * Constructor
	 */
	public function __construct() {
		$this -> request = array_merge($_GET, $_POST);
		$this -> mainView = new \Dub\Core\View\MainView();
		$this -> cssFiles = $this -> jsFilesHead = $this -> jsFilesBody = "\n";
	}

	/**
	 * Handle the package loading and controller action calling
	 * 
	 * It just takes the package information got from the localconf file of the packages and the request
	 * and handles which action on which controller should be called
	 * 
	 * @return void
	 */
	public function createMainView() {
		// TODO: This may needs some caching
		foreach($GLOBALS['dub']['packages'] as $packageKey => $package) {
			if(!$package['active']) {
				continue;
            }

			$this -> getCssfiles($package);
			$this -> getJsFilesHead($package);
			$this -> getJsFilesBody($package);

			$packageRequest = $this -> getPackageRequest($packageKey);
			$controller = $this -> getController($package, $packageRequest);
			$action = $this -> getAction($package, $packageRequest);

			// If there is no controller or action, go to the next package
			if(!$controller || !$action) {
				continue;
			}

			$this -> packageInfo[$packageKey] = $this -> getPackageInfo($controller, $action);

			$this -> runAction($packageKey, $package, $packageRequest, $controller, $action);
		}

		$this -> renderMainTemplate();
	}


	/**
	 * Get the package css files
	 *
	 * @param array $package
	 * 
	 * @return void
	 */
	protected function getCssfiles($package) {
		// Create HTML for package CSS files
		if(isset($package['localconf']['cssFiles'])) {
		    foreach($package['localconf']['cssFiles'] as $cssFile) {
			    if(file_exists($cssFile)) {
				    $this -> cssFiles .= '<link rel="stylesheet" type="text/css" href="/' . $cssFile . '" />' . "\n";
			    }
		    }
		}
	}

	/**
	 * Get the package js files loaded in the HTML head
	 *
	 * @param array $package
	 * 
	 * @return void
	 */
	protected function getJsFilesHead($package) {
		// Create HTML for package JS files loaded in the head
		if(isset($package['localconf']['jsFilesHead'])) {
		    foreach($package['localconf']['jsFilesHead'] as $jsFileHead) {
			    if(file_exists($jsFileHead)) {
				    $this -> jsFilesHead .= '<script type="text/javascript" src="/' . $jsFileHead . '"></script>' . "\n";
			    }
		    }
		}
	}

	/**
	 * Get the package js files loaded in the HTML body
	 *
	 * @param array $package
	 * 
	 * @return void
	 */
	protected function getJsFilesBody($package) {
		// Create HTML for package JS files loaded at the end of the body
		if(isset($package['localconf']['jsFilesBody'])) {
		    foreach($package['localconf']['jsFilesBody'] as $jsFileBody) {
			    if(file_exists($jsFileBody['file'])) {
				    $this -> jsFilesBody .= '<script' . (isset($jsFileBody['async']) && $jsFileBody['async'] ? ' async="async"' : '')
				    . ' type="text/javascript" src="/' . $jsFileBody['file'] . '"></script>' . "\n";
			    }
		    }
		}
	}

	/**
	 * Get the package request
	 *
	 * @param string $packageKey
	 * 
	 * @return array $packageRequest
	 */
	protected function getPackageRequest($packageKey) {
		$packageRequest = array();
		
		// Create array for package request for having a shorter way to write
		if(isset($_GET['url'])) {
			$url = explode('/', $_GET['url']);
			$startIndex = 0;
			
			// Check if the package is used
			if(isset($url[$startIndex]) && $url[$startIndex] == $packageKey) {
				// Get the controller
				if(isset($url[$startIndex + 1])) {
					$packageRequest['controller'] = $url[$startIndex + 1];
				}
				// Get controller action
				if(isset($url[$startIndex + 2])) {
					$packageRequest['action'] = $url[$startIndex + 2];
				}
				// Get the action parameters
				if(isset($url[$startIndex + 3])) {
					$packageRequest['actionParams'] = array();
					for($i = $startIndex + 3; $i < count($url); ++$i) {
						$packageRequest['actionParams'][] = $url[$i];
					}
				}
			}
		}

		// If no request for the package was found over the rewritten url get parameter, check the old school style over get parameters
		if(empty($packageRequest) && isset($this -> request[lcfirst($packageKey)])) {
			$packageRequest = $this -> request[lcfirst($packageKey)];
		}
		
		return $packageRequest;
	}

	/**
	 * Get the package controller (name) to use
	 *
	 * @param array $package
	 * @param array $packageRequest
	 * 
	 * @return string $controller
	 */
	protected function getController($package, $packageRequest) {
	    $controller = null;
	
		// Get the controller by the request, else take the default controller
		if(isset($packageRequest['controller'])) {
			$controller = $packageRequest['controller'];
		} else {
			if(isset($package['localconf']['defaultController'])) {
			    $controller = $package['localconf']['defaultController'];
			}
		}

		return $controller;
	}

	/**
	 * Get the package controller action (name) to use
	 *
	 * @param array $package
	 * @param array $packageRequest
	 * 
	 * @return string $action
	 */
	protected function getAction($package, $packageRequest) {
	    $action = null;
	
		// Get the action by the request, else take the default action
		if(isset($packageRequest['action'])) {
			$action = $packageRequest['action'];
		} else {
			if(isset($package['localconf']['defaultAction'])) {
			    $action = $package['localconf']['defaultAction'];
			}
		}

		return $action;
	}

	/**
	 * Get the package info array to render the according views
	 *
	 * @param string $controller
	 * @param array $action
	 * 
	 * @return array
	 */
	protected function getPackageInfo($controller, $action) {
		// Build package information array for displaying the packages to the frontend
		return array(
			'controller' => lcfirst($controller),
			'action' => lcfirst($action)
		);
	}

	/**
	 * Run the package controller action
	 *
	 * @param string $packageKey
	 * @param array $package
	 * @param array $packageRequest
	 * @param string $controller
	 * @param string $action
	 * 
	 * @return void
	 */
	protected function runAction($packageKey, $package, $packageRequest, $controller, $action) {
		$actionParams = array();
		$controller = '\\' . ucfirst($package['localconf']['vendor']) . '\\'
		. ucfirst($packageKey) . '\\Controller\\' . ucfirst($controller) . 'Controller';

		if(class_exists($controller)) {
			$controller = new $controller($this -> mainView -> createPackageView($packageKey));
			$action = lcfirst($action) . 'Action';

			if(method_exists($controller, $action)) {
				if(isset($packageRequest['actionParams'])) {
					$actionParams = $packageRequest['actionParams'];
				}
				call_user_func_array(array($controller, $action), $actionParams);
			}
		}
	}

	/**
	 * Render the main template
	 * 
	 * @return void
	 */
	protected function renderMainTemplate() {
		$this -> mainView -> assign('language', 'en'); // Language is static english for now
		$this -> mainView -> assign('metaData', '<meta charset="utf-8" />' ."\n");
		$this -> mainView -> assign('cssFiles', $this -> cssFiles);
		$this -> mainView -> assign('jsFilesHead', $this -> jsFilesHead);
		$this -> mainView -> assign('jsFilesBody', $this -> jsFilesBody);
		$this -> mainView -> assign('packageInfo', $this -> packageInfo);
		$this -> mainView -> renderTemplate(
			'frontend',
			'Template',
			'index'
		);
	}
}
